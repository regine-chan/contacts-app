Contacts app made for assignment nr. 3

This is a console app, run it through Visual Studio 2019 and follow the instructions.

It will prompt the user for a search phrase and match it with a preset of contacts.

If any results is present the console will print them.

The console app is wrapped in a while loop, to exit the program wait for the prompt if you want to continue. Press 0 and enter to exit or press 1 and enter to continue. If the input is invalid, the console app will stop. (Prompt appears after each search result)