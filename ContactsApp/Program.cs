﻿using System;
using System.Collections.Generic;

namespace ContactsApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initializes a contact book with preset contacts
            HashSet<Contact> contacts = new HashSet<Contact>();
            contacts.Add(new Contact("Regine", "Urtegård"));
            contacts.Add(new Contact("Reginald", "Urtehouse"));
            contacts.Add(new Contact("Reinhardt", "Giskegård"));
            contacts.Add(new Contact("Robert", "Giske"));
            contacts.Add(new Contact("Jostein", "Fjellheim"));
            contacts.Add(new Contact("Sigurd", "Nedregård"));

            // Outputs the contacts to the user for ease of use
            Console.WriteLine("Registered contacts are: ");

            foreach(Contact contact in contacts)
            {
                Console.WriteLine("First name: " + contact.firstName + " last name : " + contact.lastName);
            }

            // Sets active state to let the user search more than once
            int active = 1;

            while(active == 1)
            {
                // retrieves the search phrase, trims white space and sets to lowercase
                Console.WriteLine("Hello, please type a search phrase to search for a contact and hit enter");
                string searchPhrase = Console.ReadLine().Trim().ToLower();

                // a hashset that stores the results of the search phrase
                HashSet<Contact> results = new HashSet<Contact>();

                // loops through the contacts set and adds each match to the results set
                foreach (Contact contact in contacts)
                {
                    // Checks if first or last name contains the search phrase (everything is in lowercase)
                    if (contact.firstName.ToLower().Contains(searchPhrase) || contact.lastName.ToLower().Contains(searchPhrase))
                    {
                        results.Add(contact);                    
                    }
                }

                // Outputs the amount of results if results is greater than 0
                if(results.Count > 0)
                {
                    Console.WriteLine("\nWe found " + results.Count + " contact(s) matching the search phrase.");
                    Console.WriteLine("Is this any of the contact(s) you were looking for? \n");

                    foreach (Contact contact in results)
                    {
                        Console.WriteLine("First name: " + contact.firstName + " last name : " + contact.lastName);
                    }

                }
                // if not print sorry statement
                else
                {
                    Console.WriteLine("Couldn't find any contacts with that search phrase, please try again.\n");
                }
                
                // prompt the user if they want to continuo, exits loop if 1 is not entered
                Console.WriteLine("\nWould you like to continue looking for contacts? (type 1 to continue, 0 to exit) \n");
                
                try
                {
                    active = int.Parse(Console.ReadLine());
                } catch (Exception e)
                {
                    Console.WriteLine("\n\nHi, probably couldn't parse your input, sos, exiting program...\n\n");
                } finally
                {
                    active = 0;
                }
               
            }
        }
    }

    // Contact class to hold firstname and lastname values
    class Contact
    {
        public string firstName { get; set; }
        public string lastName { get; set; }

        public Contact(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        
    }
}

